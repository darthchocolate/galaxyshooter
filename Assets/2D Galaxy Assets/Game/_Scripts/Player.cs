﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Player : MonoBehaviour
{
    //players Health
    public int CurrentHealth { get; set; }
    public int MaxHealth { get; set; }

    public Slider healthBar;

    //Power Ups
    public bool tripleShoot;
    public bool isSpeedBoostActive = false;
    public bool hasSheild = false;

    private UIManager _uiManager;
    private GameManager _gameManager;
    private SpawnManager _spawnManager;
    private AudioSource _audioSource;

    [SerializeField]
    private GameObject _shieldPrefab;

    [SerializeField]
    private GameObject _laserPrefab;

    [SerializeField]
    private GameObject _explosionPrfab;

    [SerializeField]
    private GameObject _tripleLaserPrefab;

    [SerializeField]
    private float _speed = 5.0f;

    [SerializeField]
    private float _fireRate = .01f;

    [SerializeField]
    private float _canFire;

    [SerializeField]
    private GameObject[] _engines;

    // Use this for initialization
    private void Start()
    {
        //_uiManager = GameObject.Find("UIManager").GetComponent<UIManager>();
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();

        MaxHealth = 100;
        // Rest health on game load
        CurrentHealth = MaxHealth;

        if (GameObject.FindGameObjectWithTag("HealthSlider")) {
            healthBar = (Slider)FindObjectOfType(typeof(Slider));
        }

        if ( _spawnManager != null )
        {
            _spawnManager.StartSpawnRoutine();
        }

        if (_gameManager.isCoOpMode == false)
        {
            // CUrrent Position = new posiotn
            transform.position = new Vector3(0, 0, 0);
        }

        _audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    private void Update() {
        Movement();
        Shoot();
    }

    private void Shoot() {
        //_audioSource.Play();
        if (tripleShoot == true)
        {
            // Right Laser
            Instantiate(_tripleLaserPrefab, transform.position + new Vector3(0, 0, 0), Quaternion.identity);

        } else {
            Instantiate(_laserPrefab, transform.position + new Vector3(0, .21f, 0), Quaternion.identity);
        }

        // Constint fire
        _canFire = Time.time + _fireRate;
    }

    public void Damage( int damageValue )
    {
        if (hasSheild == true)
        {
            hasSheild = false;
            _shieldPrefab.SetActive(false);
            return;
        }

        //Deduct the damage delt from the characte's health
        CurrentHealth -= damageValue;
        healthBar.value = CurrentHealth;


        // if character is out of health
        if (CurrentHealth <= 0)
        {
            Instantiate(_explosionPrfab, transform.position, Quaternion.identity);
            _gameManager.gameOver = true;
            Destroy(this.gameObject);
            //_uiManager.ShowTitleScreen();
        }
    }

    float CalculatedHealth() {
        return CurrentHealth - MaxHealth;
    }

    private void Movement()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        if (isSpeedBoostActive == true)
        {
            transform.Translate(Vector3.right * _speed * 3.5f * horizontalInput * Time.deltaTime);
            transform.Translate(Vector3.up * _speed * 3.5f * verticalInput * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.right * _speed * horizontalInput * Time.deltaTime);
            transform.Translate(Vector3.up * _speed * verticalInput * Time.deltaTime);   
        }

        if (transform.position.y > 0)
        {
            transform.position = new Vector3(transform.position.x, 0, 0);
        }
        else if (transform.position.y < -4.32f)
        {
            transform.position = new Vector3(transform.position.x, -4.32f, 0);
        }

        if (transform.position.x > 7.4f)
        {
            transform.position = new Vector3(7.4f, transform.position.y, 0);
        }
        else if (transform.position.x < -7.4f)
        {
            transform.position = new Vector3(-7.4f, transform.position.y, 0);
        }

    }

    //power ups
    // Triple Shoot
    private void TripleShoot()
    {
        Instantiate(_laserPrefab, transform.position + new Vector3(0.55f, 0.28f, 0), Quaternion.identity);
        _canFire = Time.time + _fireRate;

        Instantiate(_laserPrefab, transform.position + new Vector3(-0.55f, 0.28f, 0), Quaternion.identity);
        _canFire = Time.time + _fireRate;
    }

    public void TripleShootPowerOn()
    {
        tripleShoot = true;
        StartCoroutine(TripleShootPowerDownRoutine());
    }

    public IEnumerator TripleShootPowerDownRoutine()
    {
        yield return new WaitForSeconds(5.0f);
        tripleShoot = false;
    }

    // Speed Boost
    public void SpeedBoostPowerUpOnRoutine()
    {
        isSpeedBoostActive = true;
        StartCoroutine(SpeedBoostPowerDownRoutine());
    }

    public IEnumerator SpeedBoostPowerDownRoutine()
    {
        yield return new WaitForSeconds(5.0f);
        isSpeedBoostActive = false;
    }

    //Shield Power Up
    public void ShieldPowerUpRoutine()
    {
        hasSheild = true;
        _shieldPrefab.SetActive(true);

    }

}
