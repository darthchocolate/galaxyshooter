﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{

    [SerializeField]
    private GameObject enemyShipPrefab;

    [SerializeField]
    private GameObject _asteroid;

    [SerializeField]
    private GameObject[] powerups;

    private GameManager _gameManager;

	// Use this for initialization
	void Start ()
    {
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();


        StartCoroutine(SpawnEnemyRoutine());
        StartCoroutine(PowerUpSpawnRoutine());
	}

    public void StartSpawnRoutine()
    {
        StartCoroutine(SpawnEnemyRoutine());
        StartCoroutine(PowerUpSpawnRoutine());

    }
	// Update is called once per frame
	void Update ()
    {
        
	}

    IEnumerator SpawnEnemyRoutine()
    {
        while (_gameManager.gameOver == false)
        {
            Instantiate(enemyShipPrefab, new Vector3(Random.Range(-7f, 7f), 7f, 0), Quaternion.identity);
            Instantiate(_asteroid, new Vector3(Random.Range(-7f, 7f), 7f, 0), Quaternion.identity);
            yield return new WaitForSeconds(2.0f);
        }
    }

    IEnumerator PowerUpSpawnRoutine()
    {

        while (_gameManager.gameOver == false)
        {
            int randomPowerup = Random.Range(0, 3);
            Instantiate(powerups[randomPowerup], new Vector3(Random.Range(-6.89f, 6.89f), 6.38f, 0), Quaternion.identity);
            yield return new WaitForSeconds(5.0f);   
        }
    }

}
