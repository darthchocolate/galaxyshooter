﻿using UnityEngine;

public class GameManager : MonoBehaviour {

    public bool gameOver = true;
    public GameObject player;
    public bool isCoOpMode = false;
    private UIManager _uiManager;

	// Use this for initialization
	private void Start () {
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
	}
	
	// Update is called once per frame
	void Update () {
        if(gameOver == true) {
            if(Input.GetKeyDown(KeyCode.Space)) {
                Instantiate(player, Vector3.zero, Quaternion.identity);
                _uiManager.HideTitleScreen();
                gameOver = false;

            }
        }
		
	}
}
