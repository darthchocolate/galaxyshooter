﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    [SerializeField]
    private GameObject _asteroid;

    [SerializeField]
    private GameObject _asteroidExplosion;

    [SerializeField]
    private float _speed = 6f;

    [SerializeField]
    private AudioClip _clip;

    private UIManager _uiManager;


	// Use this for initialization
	void Start ()
    {
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);

        if (transform.position.y <= -6.44f)
        {
            // float randomX = Random.Range(-6.89f, 6.89f);
            // transform.position = new Vector3(randomX, 6.38f, 0);

            Destroy(this.gameObject); 
        }
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Laser")
        {
            if (other.transform.parent != null)
            {
                Destroy(other.transform.parent.gameObject);
            }
            Instantiate(_asteroidExplosion, transform.position, Quaternion.identity);
            Destroy(gameObject);
            AudioSource.PlayClipAtPoint(_clip, Camera.main.transform.position, 1f);
            _uiManager.UpdateScore();
        }
        else if (other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();
            if (player != null)
            {
                _uiManager.UpdateScore();
                player.Damage(15);
            }
            Instantiate(_asteroidExplosion, transform.position, Quaternion.identity);
            AudioSource.PlayClipAtPoint(_clip, Camera.main.transform.position, 1f);
            Destroy(this.gameObject);
        }
    }
}
