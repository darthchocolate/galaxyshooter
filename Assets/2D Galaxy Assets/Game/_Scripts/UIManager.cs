﻿using UnityEngine.UI;
using UnityEngine;

public class UIManager : MonoBehaviour {

    public GameObject titleScreen;


    public Text ScoreText;
    public int Score;

    public void UpdateScore() {
        Score += 10;
        ScoreText.text = "Score: " + Score;
    }

    public void ShowTitleScreen() {
        titleScreen.SetActive(true);
    }

    public void HideTitleScreen() {
        titleScreen.SetActive(false);
        ScoreText.text = "Score: ";
    }
}
