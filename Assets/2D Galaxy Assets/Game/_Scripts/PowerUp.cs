﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{

    [SerializeField]
    private float speed = 1.0f;

    [SerializeField]
    private int PowerUpID;

    [SerializeField]
    private AudioClip _clip;

	// Update is called once per frame
	void Update ()
    {
        transform.Translate(Vector3.down * Time.deltaTime * speed);	

        if (transform.position.y < -7)
        {
            Destroy(this.gameObject);
        }
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("collided " + other.name);
        
        if (other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();
            AudioSource.PlayClipAtPoint(_clip, Camera.main.transform.position, 1f);

            if (player != null)
            {
                switch (PowerUpID)
                {
                    case 0:
                        player.TripleShootPowerOn();
                        break;
                    case 1:
                        // enable speed boost
                        player.SpeedBoostPowerUpOnRoutine();
                        break;
                    case 2:
                        player.ShieldPowerUpRoutine();
                        break;
                }
            }

            Destroy(this.gameObject);
        }

    }

}
